<?php

/**
 * @file
 * uw_cfg_async.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_cfg_async_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
